import types from './types'

const buildStore = ($http) => {
  return {
    state: {
      realms: [],
      currentRealm: null
    },
    getters: {
      realms: (state) => {
        return state.realms
      },
      hasRealms: (state) => {
        return state.realms.length > 0
      },
      currentRealm: (state) => {
        return state.currentRealm
      }
    },
    mutations: {
      [types.APP_REALMS_GET_REALMS_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.APP_REALMS_GET_REALMS_SUCCESS] (state, realms) {
        state.isWaiting = false
        state.errors = []
        state.realms = realms
      },
      [types.APP_REALMS_GET_REALMS_FAILURE] (state, exception) {
        state.isWaiting = false
        state.errors.push(JSON.stringify(exception))
      },
      [types.APP_REALMS_CHANGE_REALM_SUCCESS] (state, realm) {
        state.currentRealm = realm
      }
    },
    actions: {
      async getRealms ({commit}) {
        commit(types.APP_REALMS_GET_REALMS_REQUEST)
        try {
          const data = {
            'profilesAliases': ['me']
          }
          const response = await $http.post('/orgs/alianco.GetCitizensRealms/', data)
          commit(types.APP_REALMS_GET_REALMS_SUCCESS, response.data.citizensRealms[0].realms)
        } catch (e) {
          commit(types.APP_REALMS_GET_REALMS_FAILURE, e)
          throw e
        }
      },
      async changeCurrentRealm ({commit}, realm) {
        commit(types.APP_REALMS_CHANGE_REALM_SUCCESS, realm)
        // Get Citizens
      }
    }
  }
}

export default { buildStore }
