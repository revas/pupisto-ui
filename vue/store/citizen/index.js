import types from './types'

const buildStore = ($http) => {
  return {
    state: {
      citizens: null
    },
    getters: {
      citizens: (state) => {
        return state.citizens
      }
    },
    mutations: {
      [types.APP_CITIZENS_GET_CITIZENS_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.APP_CITIZENS_GET_CITIZENS_SUCCESS] (state, citizens) {
        state.isWaiting = false
        state.errors = []
        state.citizens = citizens
      },
      [types.APP_CITIZENS_GET_CITIZENS_FAILURE] (state, exception) {
        state.isWaiting = false
        state.errors.push(JSON.stringify(exception))
      }
    },
    actions: {
      async getCitizens ({commit}, realm) {
        commit(types.APP_CITIZENS_GET_CITIZENS_REQUEST)
        try {
          const data = {
            'realmsAliases': [realm.alias]
          }
          const response = await $http.post('/orgs/alianco.GetRealmsCitizens/', data)
          commit(types.APP_CITIZENS_GET_CITIZENS_SUCCESS, response.data.realms[0].citizens)
        } catch (e) {
          commit(types.APP_CITIZENS_GET_CITIZENS_FAILURE)
          throw e
        }
      }
    }
  }
}

export default { buildStore }
