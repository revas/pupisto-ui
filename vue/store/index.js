import ProfileModule from './profile'
import RealmModule from './realm'
import CitizenModule from './citizen'
import types from './types'

const buildStore = ($http) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false,
      options: {
        navbar: {
          realms: {
            visible: true
          }
        }
      },
      errors: [],
      isReady: false
    },
    getters: {
      isReady: (state) => {
        return !!state.isReady
      },
      hasErrors: (state) => {
        return state.errors.length > 0
      },
      errors: (state) => {
        return state.errors
      },
      options: (state) => {
        return state.options
      }
    },
    mutations: {
      [types.APP_INITIALIZE_FAILURE] (state, exception) {
        state.isWaiting = false
        state.errors.push(JSON.stringify(exception))
      },
      [types.APP_INITIALIZE_SUCCESS] (state) {
        state.isReady = true
      },
      [types.APP_SET_OPTIONS] (state, options) {
        state.options = Object.assign({}, state.options, options)
      }
    },
    modules: {
      profile: ProfileModule.buildStore($http),
      realm: RealmModule.buildStore($http),
      citizens: CitizenModule.buildStore($http)
    },
    actions: {
      async initialize ({state, commit, dispatch, getters}) {
        try {
          await dispatch('getProfile')
          const name = getters['profile'].name
          if (name) {
            await dispatch('getRealms')
          }
          commit(types.APP_INITIALIZE_SUCCESS)
        } catch (ex) {
          console.log('console error', ex)
          commit(types.APP_INITIALIZE_FAILURE, ex)
        }
      },
      async setOptions ({commit}, options) {
        try {
          commit(types.APP_SET_OPTIONS, options)
        } catch (ex) {

        }
      }
    }
  }
}

export default { buildStore }
