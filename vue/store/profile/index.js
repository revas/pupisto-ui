import types from './types'

const buildStore = ($http) => {
  return {
    state: {
      profile: null
    },
    getters: {
      profile: (state) => {
        return state.profile
      },
      isProfileComplete: (state) => {
        if (state.profile) {
          return state.profile.name !== ''
        } else {
          return null
        }
      }
    },
    mutations: {
      [types.APP_PROFILES_GET_PROFILE_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.APP_PROFILES_GET_PROFILE_SUCCESS] (state, profile) {
        state.isWaiting = false
        state.errors = []
        state.profile = profile
      },
      [types.APP_PROFILES_GET_PROFILE_FAILURE] (state, exception) {
        state.isWaiting = false
        state.errors.push(JSON.stringify(exception))
      }
    },
    actions: {
      async getProfile ({commit}) {
        commit(types.APP_PROFILES_GET_PROFILE_REQUEST)
        try {
          const data = {
            'profilesAliases': ['me']
          }
          const response = await $http.post('/profiles/animo.GetProfiles/', data)
          commit(types.APP_PROFILES_GET_PROFILE_SUCCESS, response.data.profiles[0])
        } catch (e) {
          commit(types.APP_PROFILES_GET_PROFILE_FAILURE)
          throw e
        }
      }
    }
  }
}

export default { buildStore }
