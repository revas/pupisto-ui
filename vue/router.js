import Vue from 'vue'
import Router from 'vue-router'
import _ from 'lodash'

import Default from './views/Default'
import Realm from './views/Realm'

Vue.use(Router)

export default {
  buildRouter (routes, options) {
    let _routes = []
    let redirect = {}

    if (options && options.default) {
      redirect = {
        name: options.default.name,
        params: {}
      }
      if (options.default.require && options.default.require.realm) {
        redirect.params.realm = ''
      }
    } else {
      console.error('No default route configured')
    }

    // Add routes without realm require in Default view
    _routes.push({
      path: '/',
      component: Default,
      redirect: redirect,
      children: _.filter(routes, (route) => {
        return !route.require || !route.require.realm
      })
    })

    // Add routes under realm require parent in Realm view
    _routes.push({
      path: '/r/:realm',
      props: true,
      component: Realm,
      redirect: redirect,
      children: _.filter(routes, (route) => {
        return route.require && route.require.realm
      })
    })

    // Add fallback route
    _routes.push({
      path: '*',
      redirect: redirect
    })

    console.log(_routes)

    const router = new Router({
      mode: 'history',
      routes: _routes
    })

    return router
  }
}
