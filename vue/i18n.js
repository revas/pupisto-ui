import Vue from 'vue'
import Axios from 'axios'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export const i18n = new VueI18n({
})

const loadedLanguages = []

function setI18nLanguage (lang) {
  i18n.locale = lang
  Axios.defaults.headers.common['Accept-Language'] = lang
  document.documentElement.lang = lang
  return lang
}

export function loadLanguageAsync (lang) {
  if (i18n.locale !== lang) {
    if (!loadedLanguages.includes(lang)) {
      return Axios.get(`/locales/${lang}.json`).then((response) => {
        i18n.setLocaleMessage(lang, response.data)
        loadedLanguages.push(lang)
        return setI18nLanguage(lang)
      })
    }
    return Promise.resolve(setI18nLanguage(lang))
  }
  return Promise.resolve(lang)
}
