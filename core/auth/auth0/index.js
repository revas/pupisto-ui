import auth0 from 'auth0-js'
import Cookies from 'js-cookie'

function calculateExpirationTime (expiresIn) {
  return expiresIn * 1000 + new Date().getTime()
}

class Authenticator {
  constructor (config) {
    this.webAuth = new auth0.WebAuth(config)
  }

  static config () {
    return {
      domain: null,
      clientID: null,
      redirectUri: null,
      audience: null,
      responseType: 'token id_token',
      scope: 'openid profile email'
    }
  }

  login () {
    this.webAuth.authorize()
  }

  logout () {
    this.webAuth.authorize()
  }

  static checkErrorKey (errorKey) {
    if (errorKey === 'AUTH0_LOGIN_ERROR_EMAIL_NOT_VERIFIED') {
      return 'ERROR_EMAIL_NOT_VERIFIED'
    }
    return null
  }

  async handle () {
    return new Promise((resolve, reject) => {
      this.webAuth.parseHash((err, authResult) => {
        // eslint-disable-next-line
        history.pushState('', document.title, window.location.pathname +
          window.location.search)
        if (err) {
          reject(err)
        }
        if (authResult) {
          resolve({
            accessToken: authResult.accessToken,
            expiresAt: calculateExpirationTime(authResult.expiresIn)
          })
        }
        reject(new Error('Auth0 parseHash failed'))
      })
    })
  }

  storeAccessToken (domain, accessToken, expiresAt) {
    const fromTimeToRemainingDays = (time) => {
      const now = new Date()
      const remainingTime = expiresAt - now.getTime()
      if (remainingTime < 0) {
        return 0
      } else {
        return remainingTime / (1000.0 * 60 * 60 * 24)
      }
    }

    let options = { expires: fromTimeToRemainingDays(expiresAt) }
    if (process.env.NODE_ENV !== 'development') {
      options.domain = domain
    }

    Cookies.set('access-token', accessToken, options)
    Cookies.set('expires-at', expiresAt, options)
  }

  load () {
    return {
      accessToken: Cookies.get('access-token'),
      expiresAt: Cookies.get('expires-at')
    }
  }

  clearAccessToken (domain) {
    Cookies.remove('access-token', { domain: domain })
    Cookies.remove('expires-at', { domain: domain })
  }

  refresh () {
    return new Promise((resolve, reject) => {
      this.webAuth.checkSession({}, function (err, authResult) {
        if (err) {
          reject(err)
        }
        if (authResult) {
          resolve({
            accessToken: authResult.accessToken,
            expiresAt: calculateExpirationTime(authResult.expiresIn)
          })
        }
        reject(new Error('Auth0 checkSession failed'))
      })
    })
  }
}

export default Authenticator
